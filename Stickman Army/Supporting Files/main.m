//
//  main.m
//  Stickman Army
//
//  Created by Michael Lamothe on 10/08/2014.
//  Copyright Michael Lamothe 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
